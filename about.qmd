---
title: "A propos"
image: vignette.jpg
about:
  template: jolla
  links:
    - icon: mastodon
      text: Mastodon
      href: https://mamot.fr/@dbelveze
      rel: me
    - icon: github
      text: Github
      href: https://github.com/damienbelveze

---

# A propos du titre

cimarrón désigne en espagnol des habitants de la Cima (montagnes) qui vivent une vie alternative, parfois une vie collective de fuyards qui cherchent à échapper à l'ordre établi, comme les Cimarrones, ces esclaves africains qui ont réussi à se libérer de la tutelle de leurs maîtres par force ou par ruse.

# A propos de l'auteur

Je suis conservateur à la [bibliothèque de l'Université de Rennes](https://bibliotheques.univ-rennes.fr/), en charge de la formation des étudiants à la recherche, l'évaluation et l'exploitation de l'information. 
Je suis également membre de [l'atelier breton de la donnée ARDoISE](https://scienceouverte.univ-rennes.fr/atelier-rennais-de-la-donnee-information-et-soutien-aux-equipes-de-recherche), chargé d'aider les chercheurs et chercheuses dans la gestion de leurs données de recherche.

Ce carnet de notes rend compte de mes recherches à caractère professionnel ou personnel dans les domaines de l'édition et de l'information scientifique et technique.

On me trouve principalement sur <a rel="me" href="https://mamot.fr/@dbelveze">Mastodon</a>
